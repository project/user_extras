<?php

namespace Drupal\user_extras\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class UserExtrasService for integration.
 */
class UserExtrasService {

  const USER_EXTRAS_CONFIG_NAME = 'user_extras.settings';

  const ONLY_EMAIL_CONFIG = 'user_extras_only_email';

  const AUTO_ROLE_CONFIG = 'user_extras_auto_role';

  const AUTO_ROLE_ROLES_CONFIG = 'user_extras_auto_role_roles';

  /**
   * Config Factory.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  private $config;

  private $roleStorage;

  private $onlyEmail;

  private $autoRole;

  private $autoRoleRoles;

  /**
   * UserExtrasService constructor.
   */
  public function __construct(ConfigFactory $config, EntityTypeManagerInterface $entityTypeManager) {
    $this->config = $config->get(self::USER_EXTRAS_CONFIG_NAME);
    $this->roleStorage = $entityTypeManager->getStorage('user_role');
    $this->onlyEmail = $this->config->get(self::ONLY_EMAIL_CONFIG);
    $this->autoRole = $this->config->get(self::AUTO_ROLE_CONFIG);
    $this->autoRoleRoles = $this->config->get(self::AUTO_ROLE_ROLES_CONFIG);
  }

  /**
   * Checks if "Only Email" option is enabled.
   */
  public function onlyEmailEnabled() {
    return !empty($this->onlyEmail);
  }

  /**
   * Checks if "Auto role" option is enabled.
   */
  public function autoRoleEnabled() {
    return !empty($this->autoRole);
  }

  /**
   * Returns the active selected roles to be auto assigned.
   */
  public function getAutoRoles() {
    $auto_roles = [];
    if ($this->autoRoleEnabled() && is_array($this->autoRoleRoles)) {
      $roles = $this->roleStorage->loadMultiple($this->autoRoleRoles);
      foreach ($roles as $role) {
        if ($role->status()) {
          $auto_roles[$role->id()] = $role->label();
        }
      }
    }
    return $auto_roles;
  }

}
