<?php

namespace Drupal\user_extras\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * User Extras settings.
 */
class UserExtrasConfigForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const USER_EXTRAS_CONFIG_NAME = 'user_extras.settings';

  private $roleStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManager $entityTypeManager) {
    parent::__construct($config_factory);
    $this->roleStorage = $entityTypeManager->getStorage('user_role');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_extras_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::USER_EXTRAS_CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::USER_EXTRAS_CONFIG_NAME);

    $form['user_extras_use_only_email_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Use only email'),
      '#description' => $this->t('Allows to use only the email field for user registration and user login forms.'),
      '#open' => $config->get('user_extras_only_email'),
    ];

    $form['user_extras_use_only_email_container']['user_extras_only_email'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('user_extras_only_email'),
    ];

    $form['user_extras_auto_role_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Automatic roles'),
      '#description' => $this->t('Assigns automatically roles when a new user is created.'),
      '#open' => $config->get('user_extras_auto_role'),
    ];

    $form['user_extras_auto_role_container']['user_extras_auto_role'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $config->get('user_extras_auto_role'),
    ];

    $options = [];
    $roles = $this->roleStorage->loadMultiple();
    foreach ($roles as $role) {
      if ($role->status() && $role->id() != 'anonymous' && $role->id() != 'authenticated') {
        $options[$role->id()] = $role->label();
      }
    }

    $default_roles = $config->get('user_extras_auto_role_roles');
    $default_roles = !empty($default_roles) ? $default_roles : [];
    $form['user_extras_auto_role_container']['user_extras_auto_role_roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#description' => $this->t('Selected roles will be assigned automatically when a new user is created.'),
      '#options' => $options,
      '#default_value' => $default_roles,
      '#states' => [
        'disabled' => [
          'input[name="user_extras_auto_role"]' => [
            'checked' => FALSE,
          ],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable(static::USER_EXTRAS_CONFIG_NAME)
      ->set('user_extras_only_email', $form_state->getValue('user_extras_only_email'))
      ->set('user_extras_auto_role', $form_state->getValue('user_extras_auto_role'))
      ->set('user_extras_auto_role_roles', $form_state->getValue('user_extras_auto_role_roles'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
