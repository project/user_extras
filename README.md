# User Extras module for Drupal
-------------------------------

## INTRODUCTION
---------------
User Extras is a module that provides extra functionality 
to the user registration flow. It allows to create users 
using only the email field and also assign roles automatically.

### Current Features
- Hide user name field and use only email field in 
registration and login forms.
- Select roles to be assigned automatically when a new user 
is created or self registered.

## REQUIREMENTS
---------------
The module has not any kind of special requirement.
The intend of this module is keep it simple.

## INSTALLATION
---------------
Please use the standard Drupal module installation or
`composer require drupal/user_extras`

## CONFIGURATION
----------------
User Extras module sets the name of the user with the same 
value of the email field, so the core functionality continues 
working without any alteration or custom flow.
Roles are assigned automatically before a new user is saved.

## MAINTAINERS
--------------
Current maintainers:
* Matias Miranda - https://www.drupal.org/u/matiasmiranda
